package com.example.mySQLproject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.EventListener;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Collections;
import java.util.Scanner;

@Component
public class Start implements CommandLineRunner {

    private UserRepo userRepo;

    @Autowired
    public Start(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @EventListener(ApplicationReadyEvent.class) //umozliwia uruchomienie w momencie startu aplikacji
    public void runMethod() {
        //User user = new User("Tomasz", "password123321", 156); //odwołanie sie do konstruktora parametrowego
        int counter = 0;
        while(counter<3){
            Scanner scanner2 = new Scanner(System.in);
            System.out.println("Login: ");
            String login = scanner2.nextLine();
            System.out.println("Password: ");
            String password = scanner2.nextLine();
            System.out.println("PIN: ");
            int code = scanner2.nextInt();
            User user3 = new User(login, password, code); //utworzony obiekt klasy User (zdefiniowane 3 parametry)
            userRepo.save(user3); //zapis do bazy obiektu user3
            System.out.println("User successfully added to the database! :D");
            counter++;
        }

        //User user3 = new User("login", "password", 15);
        //userRepo.save(user3); //zapis do bazy

        //ODCZYT Z BAZY:
        //Iterable<User> all = userRepo.findAll();
        //all.forEach(System.out::println);   //przepisalem to do ponizszej petni zeby miec kontole

        int temp = 1;
        do{
            Iterable<User> all2 = userRepo.findAll();  //Implementing this interface allows an object to be the target of the "for-each loop" statement https://docs.oracle.com/javase/8/docs/api/java/lang/Iterable.html
            //findAll() method allows us to get or retrieve all the entities from the database table. It belongs to the CrudRepository interface defined by Spring Data  https://www.javaguides.net/2021/12/spring-data-findall-method.html
            all2.forEach(System.out::println);//wymaga metody toString w klasie Object (User.java)

            System.out.println("Czy kończyć: [tak: 1, nie: 0]");
            Scanner scanner = new Scanner(System.in);
            temp = scanner.nextInt();

            System.out.println("Czy chcesz pobrać jeden konkretny rekord z bazy? [tak: 1, nie: 0]");
            int var = scanner.nextInt();
            while(var == 1){
                System.out.println("Podaj id rekordu: ");
                int idRecord = scanner.nextInt();
                System.out.println(userRepo.findAllById(Collections.singleton(idRecord))); //drukowanie wpisu o id=2 singleton(2L)
                System.out.println("Czy chcesz pobrac kolejny numer? [tak: 1, nie: 0]");
                int var2 = scanner.nextInt();
                var = var2;
            }
            System.out.println("KONIEC");

        }while(temp == 0);



    }

    @Override
    public void run(String... args) throws Exception {

    }
}
