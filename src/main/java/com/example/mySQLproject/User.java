package com.example.mySQLproject;


import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)//IDENTITY inkrementacja o 1
    private Integer id;

    private String login;
    private String password;
    private int value;
    //private LocalDateTime lastLogindate;

    public User(String login, String password, int value) {
        this.login = login;
        this.password = password;
        this.value = value;
    }

    //dodane:
    public String toString() {
        return String.format("User[id=%d,login='%s', password='%s', value=%d]", id, login, password, value);
    }


    public User() { //bezparametrowy konstruktor
    }

    public Integer getId(){
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}

