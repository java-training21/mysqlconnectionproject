package com.example.mySQLproject;



//komunikacja/interfejs umozliwiajacy odczyt zapis i manipulowanie danymi

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepo extends CrudRepository<User, Integer> {//kazda klasa encji musi miesc swoje własne repozytorium

}

